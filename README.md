# Convert `Cellcognition Explorer` contours to label mask


## Description

This project is about reading file containing segmentation (contours) created by `Cellcongnition Explorer` and to generate label mask for visualization


## Collaborators

- volker.hilsenstein@embl.de
- heriche@embl.de
- christian.tischer@embl.de
- arif.khan@embl.de
- sara.cuylen-haering@embl.de
- alberto.hernandez.armendariz@embl.de

## Data


## Implementation and workflow

Requirements:
- Python3
  - To install python and to create a virtual environment to run the code, please follow instructions [here](https://git.embl.de/grp-cba/cba/-/blob/master/software/Python-miniconda.md)

To install the code:
- Open terminal window
- Activate your environment
  - `conda activate your-environment-name`
- Install dependencies
  - `pip install h5py tifffile scikit-image matplotlib imagecodecs termcolor`

To run the code:
- Open terminal window
- Make sure that you are in the code folder
- Open Python by typing `python` or `ipython` in the
  - Run the code [cellcog_contour2label_2D.py](https://git.embl.de/grp-cba/cba-cellcognition-explorer-contours-to-labels/-/blob/main/code/python/cellcog_contour2label_2D.py) by typing
    - `%run cellcog_contour2label_2D.py`
  - OR type `python cellcog_contour2label_2D.py` in the terminal window
    - Run the code [cellcog_contour4movies.py](https://git.embl.de/grp-cba/cba-cellcognition-explorer-contours-to-labels/-/blob/main/code/python/cellcog_contour4movies.py) by typing
      - `%run cellcog_contour4movies.py`
    - OR type `python cellcog_contour4movies.py` in the terminal window

## Run the code on Jupyterhub

- From web browser, open [' EMBL Jupyterhub'](https://jupyterhub.embl.de/hub/spawn)
- Use your EMBL credentials to log in
- Select `Image Analysis GPU Desktop` from  `Server Options` and press `Start`
- Open the terminal window and do:
  - `git clone https://git.embl.de/grp-cba/cba-cellcognition-explorer-contours-to-labels.git` (only for the first time)
  - `cd cba-cellcognition-explorer-contours-to-labels.git`
  - `git pull`
  - `cd code/python`
  - Type script name to run the script:
    - `cellcog_contour2label_2D`
    - `cellcog_contour4movies`
- NOTE-> Please use your own group share drives for input and output data. Do not save the data on `Jupyterhub` server.
- NOTE-> Do `pip install h5py tifffile scikit-image matplotlib imagecodecs termcolor` if the code does not readily work

## Usage examples

When code is run, it prompts you to select input `.h5` file from `Cellcongnition Explorer` and to select results directory.


## Materials and methods

Describe the methods that are used. This text could be used for publication.
