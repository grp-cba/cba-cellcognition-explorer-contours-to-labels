# /**
#  *
#  * Repository:
#  *
#  *
#  * Creation:
#  * Date: June 2022
#  * By: volker.hilsenstein@embl.de & christian.tischer@embl.de & 
#  *     arif.khan@embl.de & heriche@embl.de
#  * For: sara.cuylen-haering@embl.de & alberto.hernandez.armendariz@embl.de
#  *
#  * Requirements
#  * - Python3
#  * - Python libraries: h5py, tifffile, scikit-image, matplotlib, imagecodecs, 
#  *   termcolor
#  *
#  * inputs:
#  * - .h5 file from Cellcognition explorer
#  * - Output directory to save movies
#  * 
#  * outputs:
#  * - Saves movies in individual folders
#  * - Shows images (optional)
#  * - ....
#  *
#  */

import numpy as np
import h5py
from pathlib import Path
import tifffile
import skimage
import tkinter as tk
from tkinter import ttk, filedialog
import os
import matplotlib.pyplot as plt
from termcolor import colored



# Input fetching through class GUI
class GUI:

    def __init__(self, window): 
        # 'StringVar()' is used to get the instance of input field
        self.input_text = tk.StringVar()
        self.input_text1 = tk.StringVar()
        self.show_intermediate_results = tk.BooleanVar()    
        self.path = ''
        self.path1 = ''

        window.title("Fetch inputs")
        window.geometry("1000x150")

        ttk.Button(window, 
                   text = "Select input file", 
                   command = lambda: self.set_path_input_field()).grid(row = 0,
                                                                       ipadx=5,
                                                                      ipady=10) 
        ttk.Entry(window,
                  textvariable = self.input_text, 
                  width = 100).grid( row = 0, column = 1, ipadx=1, ipady=1)

        ttk.Button(window, 
                   text = "Select output folder", 
                   command = lambda: self.set_path_output_dir()).grid(row = 1,
                                                                      ipadx=5,
                                                                      ipady=10) 
        ttk.Entry(window, 
                  textvariable = self.input_text1, 
                  width = 100).grid( row = 1,
                                    column = 1, 
                                    ipadx=1, 
                                    ipady=1)

        ttk.Checkbutton(window, 
                        text='See intermediate results', 
                        variable=self.show_intermediate_results, 
                        onvalue=True, 
                        offvalue=False).grid(row=2, column=1)
       
        ttk.Button(window, 
                   text="OK", 
                   command= lambda: self.Close()).grid(row=7, 
                                                       column=2)
        
    def set_path_input_field(self):
        self.path = filedialog.askopenfile(initialdir=os.path.expanduser('~'),parent=window,
                                           mode='r', 
                                          filetypes=[('hdf5 files', '*.hdf*')])
        self.path = self.path.name
        self.input_text.set(self.path)

    def set_path_output_dir(self):
        self.path1 = filedialog.askdirectory(initialdir=os.path.expanduser('~'),parent=window,
                                        title='select folder to save results')
        self.input_text1.set(self.path1)

    def get_file_path(self): 
        """ Function provides the Users full file path."""
        return self.path

    def get_output_dir(self):
        """Function provides the Enova full file path."""
        return self.path1
    
    def show_results(self):
        """Function provides the Enova full file path."""
        return self.show_intermediate_results.get()
    
    def Close(self):
        window.destroy()


if __name__ == '__main__':
    window = tk.Tk()
    window.attributes('-topmost',True)
    gui = GUI(window)
    window.mainloop()



# Input parameters fetched from GUI
file_path = gui.get_file_path()
save_path = gui.get_output_dir()
show_intermediate_results = gui.show_results()


dir_path = os.path.dirname(file_path)
print("Input directory: ", dir_path)
os.chdir(dir_path)

# Open h5 file
f=h5py.File(file_path, "r")
# pd.DataFrame(np.array(f["data"]["bbox"]))


# Find the contour indices
label_indices = {}
for i,item in enumerate(np.array(f["data"]["bbox"])):
    file = item[0].decode("UTF-8")
    frame = item[3]
    label_indices[(file,frame)] = label_indices.get((file,frame),[]) + [i]



# define image shape here if you know it. This saves time reading the images.
image_shape = (512,512)
# otherwise, if you don't know it comment out the line below
# image_shape = None

# convert contours to labels and save tiffs
for file, frame in label_indices:
    if image_shape is None:
        im = tifffile.imread(file) #.replace("comp.tif","C01.ome.tif"))
        image_shape= im.shape[-2:]
    labels = np.zeros(image_shape, dtype=int)
    polygons = [np.vstack((c[0], c[1])).T for c in f["data/contours/Channel_1"]
                [label_indices[(file, frame)]]]
    for p in polygons:
        r, c = p.astype(np.uint16)[:, 0], p.astype(np.uint16)[:, 1]
        rr, cc = skimage.draw.polygon(r, c)
        labels[rr, cc] = 1        
    print(colored('Current file name : ','blue'), colored(file, 'magenta'))
    # create output folder for this file, ignore errors if it exists
    outputfolder = save_path / Path(file).parent / f"{Path(file).stem}-labels"
    outputfolder.mkdir(exist_ok=True) 
    savename = outputfolder / Path(file).name.replace(".tif", 
                                          f"_labels-{str(frame).zfill(3)}.tif")
    tifffile.imwrite(savename, labels, compression="zlib")
    print(colored('File saved as : ', 'cyan'), colored(savename, 'green'))
    
    if show_intermediate_results == True:
        label_image = skimage.measure.label(labels)
        plt.imshow(label_image)
        plt.show()

