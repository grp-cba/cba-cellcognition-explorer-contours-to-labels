from tkinter import ttk, StringVar, BooleanVar
from tkinter import filedialog
import numpy as np
import h5py
from pathlib import Path
import tifffile
import skimage
import tkinter as tk
import os
import matplotlib.pyplot as plt
from termcolor import colored

class GUI:

    def __init__(self, window): 
        # 'StringVar()' is used to get the instance of input field
        self.input_text = StringVar()
        self.input_text1 = StringVar()
        self.show_intermediate_results = BooleanVar()
        

        self.path = ''
        self.path1 = ''

        window.title("Fetch inputs")
        window.resizable(0, 0) # this prevents from resizing the window
        window.geometry("800x250")

        ttk.Button(window, text = "Select input file", command = lambda: self.set_path_input_field()).grid(row = 0, ipadx=5, ipady=15) # this is placed in 0 0
        ttk.Entry(window, textvariable = self.input_text, width = 70).grid( row = 0, column = 1, ipadx=1, ipady=1) # this is placed in 0 1

        ttk.Button(window, text = "Select output directory", command = lambda: self.set_path_output_dir()).grid(row = 1, ipadx=5, ipady=15) # this is placed in 0 0
        ttk.Entry(window, textvariable = self.input_text1, width = 70).grid( row = 1, column = 1, ipadx=1, ipady=1) # this is placed in 0 1

        ttk.Checkbutton(window, text='See intermediate results', variable=self.show_intermediate_results, onvalue=True, offvalue=False).grid(row=6, column=0)
        ttk.Button(window, text="OK", command= lambda: self.Close()).grid(row=7, column=0)
        
    def set_path_input_field(self):
        self.path = filedialog.askopenfile(parent=window, mode='r', filetypes=[('hdf5 files', '*.hdf*')])
        self.path = self.path.name
        self.input_text.set(self.path)

    def set_path_output_dir(self):
        self.path1 = filedialog.askdirectory(parent=window, title='select directory to save results')
        self.input_text1.set(self.path1)

    def get_file_path(self): 
        """ Function provides the Users full file path."""
        return self.path

    def get_output_dir(self):
        """Function provides the Enova full file path."""
        return self.path1
    
    def show_results(self):
        """Function provides the Enova full file path."""
        return self.show_intermediate_results.get()
    
    def Close(self):
        window.destroy()


if __name__ == '__main__':
    window = tk.Tk()
    window.attributes('-topmost',True)
    gui = GUI(window)
    window.mainloop()
    
    # Extracting the full file path for re-use. Two ways to accomplish this task is below. 
    print(gui.path, '\n', gui.path1) 
    print(gui.get_file_path(), '\n', gui.get_output_dir())
    print(gui.show_results())
    
dir_path = os.path.dirname(gui.get_file_path())
print("Input directory: ", dir_path)
os.chdir(dir_path)

# Open h5 file
f=h5py.File(gui.get_file_path(), "r")
# pd.DataFrame(np.array(f["data"]["bbox"]))


# Find the contour indices
contour_indices_per_file_and_frame = {}
for i,item in enumerate(np.array(f["data"]["bbox"])):
    file = item[0].decode("UTF-8")
    frame = item[3]
    contour_indices_per_file_and_frame[(file, frame)] = contour_indices_per_file_and_frame.get((file,frame),[]) + [i]



# define image shape here if you know it. This saves time reading the images.
image_shape = (512,512)
# otherwise, if you don't know it comment out the line below
# image_shape = None

# convert contours to labels and save tiffs
for file, frame in contour_indices_per_file_and_frame:
    if image_shape is None:
        im = tifffile.imread(file) #.replace("comp.tif","C01.ome.tif"))
        image_shape= im.shape[-2:]
    labels = np.zeros(image_shape, dtype=int)
    polygons = [np.vstack((c[0], c[1])).T for c in f["data/contours/Channel_1"][contour_indices_per_file_and_frame[(file, frame)]]]
    for p in polygons:
        r, c = p.astype(np.uint16)[:, 0], p.astype(np.uint16)[:, 1]
        rr, cc = skimage.draw.polygon(r, c)
        labels[rr, cc] = 1        
    print(colored('Current file name : ','blue'), colored(file, 'magenta'))
    # create output folder for this file, ignore errors if it exists
    outputfolder = gui.get_output_dir() / Path(file).parent / f"{Path(file).stem}-labels"
    outputfolder.mkdir(exist_ok=True) 
    savename = outputfolder / Path(file).name.replace(".tif", f"_labels-{str(frame).zfill(3)}.tif")
    tifffile.imwrite(savename, labels, compression="zlib")
    print(colored('File saved as : ', 'cyan'), colored(savename, 'green'))
    
    if gui.show_results() == True:
        label_image = skimage.measure.label(labels)
        plt.imshow(label_image)
        plt.show()