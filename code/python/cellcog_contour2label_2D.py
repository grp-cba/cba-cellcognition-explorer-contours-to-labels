#  * Repository:
#  *
#  *
#  * Creation:
#  * Date: June 2022
#  * By: volker.hilsenstein@embl.de & christian.tischer@embl.de & 
#  *     arif.khan@embl.de & heriche@embl.de
#  * For: sara.cuylen-haering@embl.de & alberto.hernandez.armendariz@embl.de
#  *
#  * Requirements
#  * - Python3
#  * - Python libraries: h5py, tifffile, scikit-image, matplotlib, imagecodecs,
#  *   termcolor
#  *
#  * inputs:
#  * - .h5 file from Cellcognition explorer
#  * - Output directory to save images
#  * 
#  * outputs:
#  * - Saves label masks in output directory
#  * - Saves overlaid images with outlines in output directory
#  * - ....
#  *
#  */

import numpy as np
import h5py
import tifffile
import skimage
import tkinter as tk
from tkinter import ttk, filedialog
import os
import matplotlib.pyplot as plt
from termcolor import colored



# Input fetching through class GUI
class GUI:

    def __init__(self, window): 
        # 'StringVar()' is used to get the instance of input field
        self.input_text = tk.StringVar()
        self.input_text1 = tk.StringVar()
        self.show_intermediate_results = tk.BooleanVar()
        self.path = ''
        self.path1 = ''

        window.title("Fetch inputs")
        window.geometry("1000x150")

        ttk.Button(window, 
                   text = "Select input file", 
                   command = lambda: self.set_path_input_field()).grid(row = 0,
                                                                       ipadx=5,
                                                                       ipady=10)
        ttk.Entry(window, 
                  textvariable = self.input_text, width = 100).grid( row = 0, 
                                                                    column = 1,
                                                                    ipadx=1, 
                                                                    ipady=1) 

        ttk.Button(window, 
                   text = "Select output folder", 
                   command = lambda: self.set_path_output_dir()).grid(row = 1, 
                                                                      ipadx=5, 
                                                                      ipady=10) 
        ttk.Entry(window, 
                  textvariable = self.input_text1, 
                  width = 100).grid( row = 1, column = 1, ipadx=1, ipady=1) 

        ttk.Checkbutton(window, 
                        text='See intermediate results', 
                        variable=self.show_intermediate_results, 
                        onvalue=True, 
                        offvalue=False).grid(row=2, column=1)
        
        ttk.Button(window, 
                   text="OK", 
                   command= lambda: self.Close()).grid(row=7, column=2)
        
    def set_path_input_field(self):
        self.path = filedialog.askopenfile(initialdir=os.path.expanduser('~'),parent=window, 
                                           mode='r', 
                                          filetypes=[('hdf5 files', '*.hdf*')])
        self.path = self.path.name
        self.input_text.set(self.path)

    def set_path_output_dir(self):
        self.path1 = filedialog.askdirectory(initialdir=os.path.expanduser('~'),parent=window, 
                                        title='select folder to save results')
        self.input_text1.set(self.path1)

    def get_file_path(self): 
        """ Function provides the Users full file path."""
        return self.path

    def get_output_dir(self):
        """Function provides the Enova full file path."""
        return self.path1
    
    def show_results(self):
        """Function provides the Enova full file path."""
        return self.show_intermediate_results.get()
    
    def Close(self):
        window.destroy()


if __name__ == '__main__':
    window = tk.Tk()
    window.attributes('-topmost',True)
    gui = GUI(window)
    window.mainloop()



# Input parameters fetched from GUI
file_path = gui.get_file_path()
save_path = gui.get_output_dir()
show_intermediate_results = gui.show_results()
print("Selected file: ", file_path)
dir_path = os.path.dirname(file_path)
print("Input directory: ", dir_path)
os.chdir(dir_path)

# Open h5 file
f=h5py.File(file_path, "r")

# Find the contour indices
contour_indices_per_file = {}
for i,item in enumerate(np.array(f["data"]["bbox"])):
    file = item[0].decode("UTF-8")
    contour_indices_per_file[file] = contour_indices_per_file.get(file,[]) + [i]

# convert contours to labels and display overlay
for file in contour_indices_per_file:
    im = tifffile.imread(file.replace("comp.tif","C01.ome.tif"))
    labels = np.zeros(im.shape, dtype=int)
    outlines = np.zeros(im.shape, dtype=int)
    polygons = [np.vstack((c[0], c[1])).T for c in f["data/contours/Channel_1"]
                [contour_indices_per_file[file]]]
    for p in polygons:
        r, c = p.astype(np.uint16)[:, 0], p.astype(np.uint16)[:, 1]
        rr, cc = skimage.draw.polygon(r, c)
        labels[rr, cc] = 1
        outlines[rr, cc] = 1
    savename = os.path.join(save_path, file.replace("comp.tif", "labels.tif"))
    tifffile.imwrite(savename, labels)
    print(colored('Label mask is saved as : ', 'magenta'), 
          colored(savename, 'green'))
    label_image = skimage.measure.label(outlines)    
    image_label_overlay = skimage.color.label2rgb(label_image, image=im)
    image_label_outlines_overlay = skimage.segmentation.mark_boundaries(im, 
                                                                        labels,
                                                                  mode='thick')
    savename = os.path.join(save_path, 
                            file.replace("comp.tif", "overlaid-outlines.tif"))
    tifffile.imwrite(savename, image_label_outlines_overlay)
    print(colored('Overlay with segmentation outlines are saved as : ', 
                  'cyan'), 
          colored(savename, 'green'))
    
    if show_intermediate_results == True:
        plt.imshow(image_label_outlines_overlay)
        plt.show()


